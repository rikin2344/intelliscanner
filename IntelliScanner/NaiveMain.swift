//
//  NaiveMain.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 10/12/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

var IGNORE_COMMON = 0.1
var IGNORE_RARE_WORDS = 0.01

class NaiveMain {
    
    var CLASSIF_THRESH = 0;
    var THRESHOLD = 14
    var train = ""
    var test = ""
    
    var testFalsePosMat = 0.0
    var trainErrorMat = 0.0
    var testErrorMat = 0.0

    var bayes = NaiveBayes()
    
    func run(){
        self.getTrainTestFiles()
        self.runNaiveBayes()
    }
    
    private func getTrainTestFiles(){
        if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String] {
            let dir = dirs[0] //documents directory
            
            let trainPath = dir.stringByAppendingPathComponent("train3.txt");
            self.train = String(contentsOfFile: trainPath, encoding: NSUTF8StringEncoding, error: nil)!

            let testPath = dir.stringByAppendingPathComponent("test3.txt");
            self.test = String(contentsOfFile: testPath, encoding: NSUTF8StringEncoding, error: nil)!
        }
    }
    
    private func runNaiveBayes(){
        bayes.naiveBayes(self.train, test: self.test, bias: false, threshold: self.THRESHOLD)
    }
        
}


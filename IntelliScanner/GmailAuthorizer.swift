//
//  GmailAuthorizer.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/18/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

private let kKeychainItemName = "Gmail API"
private let kClientID = "86276676584-ikt6bf9dfadu55f20709pfl8t8gjifcq.apps.googleusercontent.com"
private let kClientSecret = "W-0vMGPl2DgQd76ncSDrvaPc"
let scopes = [kGTLAuthScopeGmailReadonly]
let service = GTLServiceGmail()


class GmailAuthorizer{
    

    class func viewDidLoad(){
        GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
            kKeychainItemName,
            clientID: kClientID,
            clientSecret: kClientSecret
        )
    }
    
    // Creates the auth controller for authorizing access to Gmail API
    func createAuthController(completion: Void -> Void) -> GTMOAuth2ViewControllerTouch {
        let scopeString = " ".join(scopes)
        return GTMOAuth2ViewControllerTouch(scope: scopeString, clientID: kClientID, clientSecret: kClientSecret, keychainItemName: kKeychainItemName, completionHandler: { (vc, authResult, error) -> Void in
            
            // Handle completion of the authorization process, and update the Gmail API
            // with the new credentials.
            
            if let error = error {
                service.authorizer = nil
                self.showAlert("Authentication Error", message: error.localizedDescription)
                return
            }
            
            service.authorizer = authResult
            completion()
        })
    }
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK"
        )
        alert.show()
    }

    
}
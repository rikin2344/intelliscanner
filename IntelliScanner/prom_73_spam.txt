Free Shipping on all orders over &#36;50 See details Abercrombie &amp; Fitch New York The Long Weekend Event - Entire Store 40-60% Off - In Stores &amp; Online* Shop Mens Shop Womens *Online price reflects discount. Excludes select New Arrivals, A&F Essentials, Fragrance, and 3rd party. See Details All Clearance up to 70% Off - Exclusively Online* Shop Mens Shop Womens *Price reflects discount. See Details Abercrombie Kids Shop Now mens womens jeans lookbook a&amp;f kids facebook twitter instagram pinterest store locator

FROM: "Abercrombie & Fitch" 
SUBJECT: The ENTIRE store is 40-60% off... it’s our Long Weekend Event!

//
//  FirstViewController.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/17/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    private let gmailAuthorizer = GmailAuthorizer()
    private var EmailCount = 500
    private var queryCount = 1
    var documentDir:String?
    
    // When the view loads, create necessary subviews
    // and initialize the Gmail API service
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GmailAuthorizer.viewDidLoad()
        if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String] {
            let dir = dirs[0] //documents directory
            self.documentDir = dir
        }


    }
    
    // When the view appears, ensure that the Gmail API service is authorized
    // and perform API calls
    override func viewDidAppear(animated: Bool) {
        if let authorizer = service.authorizer,
            canAuth = authorizer.canAuthorize where canAuth {
                println("Authentication Successfull")
                println("Now fetching mails..")
                fetchMails(nil)
        } else {
            presentViewController(gmailAuthorizer.createAuthController({
                self.dismissViewControllerAnimated(true, completion: nil)
            }), animated: true, completion: nil)
            
        }
    }
    
    func fetchMails(queryParam: GTLQueryGmail?){
        let query: GTLQueryGmail!
        if (queryParam == nil){
            query = GTLQueryGmail.queryForUsersMessagesList()
        }else{
            query = queryParam
        }
        println(query.pageToken)
        service.executeQuery(query, completionHandler: { (ticket, response, error) -> Void in
            if let error =  error {
                self.showAlert("Error", message: error.localizedDescription)
                return
            }else{
                let mesageListResponse = response as! GTLGmailListMessagesResponse
                let messageLists = mesageListResponse.messages as! [GTLGmailMessage]
                
                if(self.queryCount <= 5){
                    let nextPageToken = mesageListResponse.nextPageToken
                    let newQuery = GTLQueryGmail.queryForUsersMessagesList()
                    newQuery.pageToken = nextPageToken
                    self.fetchMails(newQuery)
//                    ++self.queryCount
                }
                
                println("Downloading \(messageLists.count) email's content")
                for list in messageLists{
                    let emailQuery = GTLQueryGmail.queryForUsersMessagesGet()
                    emailQuery.identifier = list.identifier
                    emailQuery.format = "full"
                    service.executeQuery(emailQuery, completionHandler: { (ticket2, response2, error2) -> Void in
                        if let content = response2, json = content.JSON {
                            if let extractedContent = extractContent(param: 0, json) as? String {
                                self.writeToFile(extractedContent)
                            }
                        }
                    })
                }
            }
        })
    }
    
    func writeToFile(content: String){
        
        let file = "prom_\(self.EmailCount)_spam.txt"
        
        self.EmailCount++
        if(self.EmailCount == 2){
            println(self.documentDir!.stringByAppendingPathComponent(file))
        }
        var path = self.documentDir!.stringByAppendingPathComponent(file)
        
        var filteredContent = ""
        
        //Extracted the content of ALT tag
        var match = matchesForRegexInString("(?<=\\balt=\")[^\"]*", content)
        
        if(match.count == 0) {
            return
        }
        
        filteredContent = (" ").join(match)
        
        //TO GET THE TAGLess STRING
        let regex:NSRegularExpression  = NSRegularExpression(
            pattern: "<.*?>",
            options: NSRegularExpressionOptions.CaseInsensitive,
            error: nil)!
        
        
        let range = NSMakeRange(0, count(content))
        let htmlLessString :String = regex.stringByReplacingMatchesInString(content,
            options: NSMatchingOptions.allZeros,
            range:range ,
            withTemplate: "")

        filteredContent = filteredContent + "\n\n" + htmlLessString
        
        var tempContent = ""
        var tagFound = false
        var canEdit = false
        
        for line in filteredContent.componentsSeparatedByString("\n"){
            
            if(line.rangeOfString("TO:") != nil ){
                continue
            }
            
            if(line.rangeOfString("MESSAGE") != nil ){
                break
                //canEdit = true
                //continue
            }
            
            if(canEdit){
                //Getting rid of extra characters
                var filteredLine = line.stringByReplacingOccurrencesOfString("&nbsp;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                filteredLine = line.stringByReplacingOccurrencesOfString("&amp;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                
                //getting rid of extra HTML tags
                if filteredLine.rangeOfString(">") != nil {
                    tagFound = false
                    continue
                }
                
                if filteredLine.rangeOfString("<") != nil {
                    tagFound = true
                    continue
                }
                
                //getting rid of CSS
                if filteredLine == "" || tagFound || filteredLine.rangeOfString("/*") != nil ||  filteredLine.rangeOfString("{") != nil
                    || filteredLine.rangeOfString("}") != nil || filteredLine.rangeOfString(";") != nil || filteredLine.rangeOfString("href") != nil{
                        continue
                }
                
                if filteredLine.rangeOfString("This email is not endorsed") != nil{
                    break
                }
                
                if filteredLine.rangeOfString("Search over") != nil{
                    break
                }
                
                if filteredLine.rangeOfString("*Online Only.") != nil || filteredLine.rangeOfString("**Offer only") != nil{
                    break
                }
                
                
                tempContent = tempContent + filteredLine + "\n"
            }else{
                tempContent = tempContent + line + "\n"
            }
        }
        
        filteredContent = tempContent
        
        filteredContent.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
    }
    
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK"
        )
        alert.show()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
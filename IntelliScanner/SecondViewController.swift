//
//  SecondViewController.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/17/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var  process = Process()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        process.getFileList()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


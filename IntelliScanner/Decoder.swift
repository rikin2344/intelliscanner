//
//  Decoder.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/18/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

class Decoder {
    class func getDecodedString(#encodedString: String)->String?{
        var base64EncodedString = Decoder().convertBase64URLtoBase64(encodedString: encodedString)
        if let decodedData = NSData(base64EncodedString: base64EncodedString, options:NSDataBase64DecodingOptions(rawValue: 0)){
            return NSString(data: decodedData, encoding: NSUTF8StringEncoding) as? String
        }
        return nil
    }
    
    private func convertBase64URLtoBase64(#encodedString: String)->String{
        var tempEncodedString = encodedString.stringByReplacingOccurrencesOfString("-", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
        tempEncodedString = tempEncodedString.stringByReplacingOccurrencesOfString("_", withString: "/", options: NSStringCompareOptions.LiteralSearch, range: nil)
        var equalsToBeAdded = (encodedString as NSString).length % 4
        if(equalsToBeAdded > 0){
            for _ in 0..<equalsToBeAdded {
                tempEncodedString += "="
            }
        }
        return tempEncodedString
    }
    
}
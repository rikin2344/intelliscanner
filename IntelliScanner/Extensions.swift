//
//  Extensions.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/23/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

extension String {
    var words:[String] {
        return "".join(componentsSeparatedByCharactersInSet(NSCharacterSet.punctuationCharacterSet())).componentsSeparatedByString(" ").filter{$0 != ""}
    }
    
    func clean()->String{
        return self.stringByReplacingOccurrencesOfString("\r\n", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}

extension Character{
    func isupper() -> Bool{
        let cs = String(self)
        return (cs == cs.uppercaseString) && (cs != cs.lowercaseString)
    }
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}

func addDict(var dict:[String:Double],dictToBeAdded:[String:Double])->[String:Double]{
    for (key,value) in dictToBeAdded{
        if let v = dict[key]{
            var newValue = value + v
            dict.updateValue(newValue, forKey: key)
        }else{
            dict[key] = value
        }
    }
    return dict
}

func matchesForRegexInText(regex: String!, text: String!) -> [String] {
    let regex = NSRegularExpression(pattern: regex,
        options: nil, error: nil)!
    let nsString = text as NSString
    let results = regex.matchesInString(text,
        options: nil, range: NSMakeRange(0, nsString.length))
        as! [NSTextCheckingResult]
    return map(results) { nsString.substringWithRange($0.range)}
}


func shuffle<C: MutableCollectionType where C.Index == Int>(var list: C) -> C {
    let c = count(list)
    if c < 2 { return list }
    for i in 0..<(c - 1) {
        let j = Int(arc4random_uniform(UInt32(c - i))) + i
        swap(&list[i], &list[j])
    }
    return list
}

func matchesForRegexInString(regex: String!, text: String!) -> [String] {
    
    let regex = NSRegularExpression(pattern: regex,
        options: nil, error: nil)!
    let nsString = text as NSString
    let results = regex.matchesInString(text,
        options: nil, range: NSMakeRange(0, nsString.length))
        as! [NSTextCheckingResult]
    return map(results) { nsString.substringWithRange($0.range)}
}

struct Array2D<T : IntegerLiteralConvertible > {
    let rows : Int
    let cols : Int
    var matrix: [T]
    
    init(rows : Int, cols : Int) {
        self.rows = rows
        self.cols = cols
        matrix = Array(count : rows * cols, repeatedValue : 0)
    }
    
    subscript(row : Int, col : Int) -> T {
        get { return matrix[cols * row + col] }
        set { matrix[cols*row+col] = newValue }
    }
}

extension Array2D {
    
    init(_ elements: [[T]]) {
        let rows = elements.count
        let cols = elements[0].count
        self.init(rows: rows, cols: cols)
        for i in 0 ..< rows {
            assert(elements[i].count == cols, "Array must have same number of elements for each row")
            self.matrix.replaceRange(cols * i ..< cols * (i+1), with: elements[i])
        }
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}

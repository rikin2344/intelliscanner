//
//  NaiveBayes.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 10/12/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

class NaiveBayes {
    
    func naiveBayes(train: String, test: String, bias: Bool, threshold: Int){
        
        //TARGET
        var Y = [Int]()
        var testY = [Int]()
        
        //TARGET FOR TRAIN
        for line in train.componentsSeparatedByString("\n"){
            if(line != ""){
                Y.append(2*(("".join(matchesForRegexInText("^*[0,1]$", line))).toInt()!) - 1)
            }
        }
        
        //TARGET FOR TEST
        for line in test.componentsSeparatedByString("\n"){
            if(line != ""){
                testY.append(2*(("".join(matchesForRegexInText("^*[0,1]$", line))).toInt()!) - 1)
            }
        }
        
        //INPUT FEATURE
        var X = [String]()
        var testX = [String]()
        var regex = NSRegularExpression(pattern: "^*,[0,1]$", options: NSRegularExpressionOptions.AllowCommentsAndWhitespace, error: nil)
        var regex2 = NSRegularExpression(pattern: "\\w\\.\\w\\w++", options: NSRegularExpressionOptions.AllowCommentsAndWhitespace, error: nil)

        //INPUT FOR TRAIN
        for line in train.componentsSeparatedByString("\n"){
            if (line != ""){
                var XString = regex!.stringByReplacingMatchesInString(line, options: nil, range: NSMakeRange(0, count(line)), withTemplate: "")
                X.append(regex2!.stringByReplacingMatchesInString(XString, options: nil, range: NSMakeRange(0, count(XString)), withTemplate: "1"))
            }
        }

        //INPUT FOR TEST
        for line in test.componentsSeparatedByString("\n"){
            if (line != ""){
                var textXString = regex!.stringByReplacingMatchesInString(line, options: nil, range: NSMakeRange(0, count(line)), withTemplate: "")
                testX.append(regex2!.stringByReplacingMatchesInString(textXString, options: nil, range: NSMakeRange(0, count(textXString)), withTemplate: "1"))
            }
        }
        
        var spam = [String]()
        var ham = [String]()
        for (index, value) in enumerate(Y){
            if(value == 1){
                spam.append(X[index])
            }else{
                ham.append(X[index])
            }
        }
        
        //CALCULATING MEAN WS
        var ws = [Double](count: 150, repeatedValue: 0.0)
        for lines in spam {
            for (index,eachValueFromLine) in enumerate(lines.componentsSeparatedByString(",")){
                let DValue = NSNumberFormatter().numberFromString(eachValueFromLine)?.doubleValue
                
                //YET TO TAKE CARE OF
                if(DValue == nil){
                }
                
                ws[index] = ws[index] + (DValue ?? 0.0)
            }
        }
        
        for (index,value) in  enumerate(ws) {
            ws[index] = value / Double(spam.count)
        }

        //CALCULATING MEAN WH
        var wh = [Double](count: 150, repeatedValue: 0.0)
        for lines in ham {
            for (index,eachValueFromLine) in enumerate(lines.componentsSeparatedByString(",")){
                let DValue = NSNumberFormatter().numberFromString(eachValueFromLine)?.doubleValue
                wh[index] = wh[index] + (DValue ?? 0.0)
            }
        }
        
        for (index,value) in  enumerate(wh) {
            wh[index] = value / Double(ham.count)
        }
        
        var HAM_DICT_SIZE = 250000
        var SPAM_DICT_SIZE = HAM_DICT_SIZE * 10
        var AVG_WORDS_IN_MSG = 100
        
        //1 - (((HAM_DICT_SIZE - 1)/HAM_DICT_SIZE) << (AVG_WORDS_IN_MSG - 1));
        var HAM_WORD_PROB = 0.00039992
        
        //1 - ((SPAM_DICT_SIZE - 1)/SPAM_DICT_SIZE) << (AVG_WORDS_IN_MSG - 1);
        var SPAM_WORD_PROB = 0.000039999
        
        for (index1,value) in enumerate(ws) {
            if (value == 0.0){
                ws[index1] = Double(SPAM_WORD_PROB)
            }
        }
        for (index2,value) in enumerate(wh) {
            if (value == 0.0){
                wh[index2] = Double(HAM_WORD_PROB)
            }
        }
/*
        //ELIMINATING THE COMMON FEATURES
        var trueInd = [Int](count: 150, repeatedValue: 1)
        var common = [Int]()
        if IGNORE_COMMON > 0 {
            for (index,value) in enumerate(ws){
                common.append(  (abs((value / (value + wh[index])) - 0.5) > IGNORE_COMMON) ? 1 : 0 )
            }
            var ncommon = common.filter({ $0 == 0 }).count
            if(ncommon > 0){
                println("Eliminated \(ncommon) common of 150 features")
            }
            for (index,value) in enumerate(trueInd){
                trueInd[index] = (value == common[index] ? value : 0)
            }
        }
        
        var rare = [Int]()
        if IGNORE_RARE_WORDS > 0 {
            for (index,value) in enumerate(ws){
                rare.append(  ( (value + wh[index]) > IGNORE_RARE_WORDS) ? 1 : 0 )
            }
            var nrare = rare.filter({ $0 == 0 }).count
            if(nrare > 0){
                println("Eliminated \(nrare) rare of 150 features")
            }
            for (index,value) in enumerate(trueInd){
                trueInd[index] = (value == rare[index] ? value : 0)
            }
        }
        
        for index4 in 0..<trueInd.count{
            if(trueInd[index4] == 0){
                ws.removeAtIndex(index4)
                wh.removeAtIndex(index4)
                for (Xindex,line) in enumerate(X) {
                    var tempLine = (line.componentsSeparatedByString(","))
                    tempLine.removeAtIndex(index4)
                    X[Xindex] = ",".join(tempLine)
                }
                for (testXindex,line) in enumerate(testX) {
                    var tempLine = (line.componentsSeparatedByString(","))
                    tempLine.removeAtIndex(index4)
                    testX[testXindex] = ",".join(tempLine)
                }
            }
        }
*/
//      ASSUMING IT IS BIASED
        var testSpamProp  = 0.5;
        var trainSpamProp = 0.5;
        
        println("TRAINING: ")
        calculateError(X, Y: Y, ws: ws, wh: wh, spamProbability: trainSpamProp, threshold: 0, isTrain: true)
        
        println()
        
        println("TESTING: ")
        
        println("THRESHOLD 0")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: 0,isTrain: false)
        println("THRESHOLD 2")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: 2,isTrain: false)
        println("THRESHOLD 4")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: 4,isTrain: false)
        println("THRESHOLD 1")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: 1,isTrain: false)
        println("THRESHOLD -1")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: -1,isTrain: false)
        println("THRESHOLD -2")
        calculateError(testX, Y: testY, ws: ws, wh: wh, spamProbability: testSpamProp, threshold: -2,isTrain: false)
    }
    
    
    func calculateError(X:[String],Y:[Int],ws:[Double], wh:[Double],spamProbability: Double, threshold: Int, isTrain: Bool){
        var numRows = X.count
        var falseNeg:Int = 0
        var falsePos:Int = 0
        
        for index in 0..<numRows{
            var sum:Double = 0.0
            var XValue = X[index]
            for (i,value) in enumerate(XValue.componentsSeparatedByString(",")){
                let DValue = NSNumberFormatter().numberFromString(value)?.doubleValue
                if DValue > 0.0 {
                    sum = sum + log(ws[i]/wh[i])
                }
            }
            var ratio = log(spamProbability/(1 - spamProbability)) + sum
            var classification = ratio > Double(threshold) ? 1 : -1
            falseNeg = falseNeg + (classification == -1 && Y[index] == 1 ? 1 : 0)
            falsePos = falsePos + (classification == 1 && Y[index] == -1 ? 1 : 0)
        }
        
        var errorRate = Double(falsePos + falseNeg) / Double(numRows)
        var noOfHams = Y.filter({ $0 == -1 }).count
        var noOfSpam = Y.filter({ $0 == 1 }).count
        var falsePosRatio = Double(falsePos) / Double(noOfHams)
        
        println("Error Rate: \((errorRate*100).roundToPlaces(3))%")
        println("Number of Ham Messages: \(noOfHams)")
        println("Number of Spam Messages: \(noOfSpam)")
        if(!isTrain){
            println("False Positive Ratio: \(falsePosRatio.roundToPlaces(3))")
        }
        println()
    }
    
    
}
























//            if let decision = line.componentsSeparatedByString(",").last where decision != ""{
//                testY.append(2*(decision.toInt()!) - 1)
//            }


//        var meanArray = [Int](count: spam.count, repeatedValue: 0)
//        for lineInSpam in spam{
//            for (index,value) in enumerate(lineInSpam.componentsSeparatedByString(",")){
//                meanArray[index] = (meanArray[index] + value.toInt()!)
//            }
//        }

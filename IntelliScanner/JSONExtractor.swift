//
//  JSONExtractor.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/18/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation

func extractContent(#param: Int, json: NSMutableDictionary)->AnyObject?{
    if param == 0{
        return extractContentToString(json)
    }else{
        return extractContentToDictionary(json)
    }
}

private func extractContentToString(json: NSMutableDictionary)->String?{
    var dataString = ""
    let (message, to, from, subject) = extractNow(json)
    if(filterSender(from)){
        if let message = message{
            return "TO: " + to + "\n" + "FROM: " + from + "\n" + "SUBJECT: " + subject + "\n" + "MESSAGE: " + message
        }
        return "TO: " + to + "\n" + "FROM: " + from + "\n" + "SUBJECT: " + subject + "\n"
    }
    return nil
}

private func filterSender(from: String)->Bool{
//    if from.lowercaseString.rangeOfString("tiger") != nil || from.lowercaseString.rangeOfString("dick's") != nil {
//        //|| from.lowercaseString.rangeOfString("cheap") != nil || from.lowercaseString.rangeOfString("spirit") != nil
//        return true
//    }
    if from.lowercaseString.rangeOfString("american") != nil{
//        || from.rangeOfString("Hollister") != nil
        return true
    }
    return false
}

private func extractContentToDictionary(json: NSMutableDictionary)->[String: String]{
    let (message, to, from, subject) = extractNow(json)
    var dictionary = [String: String]()
    if let message = message{
        dictionary["message"] = message
    }
    dictionary["to"] = to
    dictionary["from"] = from
    dictionary["subject"] = subject
    return dictionary
}

//Return a tuple
private func extractNow(json: NSMutableDictionary)->(String?, String, String, String){
    let message = extractMessage(json)
    let (to,from,subject) = extractToFromSubject(json)
    return (message, to, from, subject)
}

private func extractMessage(json: NSMutableDictionary)->String?{
    if let message1 = (((json["payload"])!["body"])!!["data"]) as? String{
        return Decoder.getDecodedString(encodedString: message1)!
    }else{
        if(json["payload"] == nil && json["payload"]!["parts"] == nil){
            return nil
        }
        if let jsonArray:NSArray = (((json["payload"])!["parts"])!! as? NSArray) {
            if jsonArray.count > 1{
                if let message = (((jsonArray[1])["body"])!!["data"]!) as? String{
                    return Decoder.getDecodedString(encodedString: message)!
                }
            }
            if let message = (((jsonArray[0])["body"])!!["data"]!) as? String{
                return Decoder.getDecodedString(encodedString: message)!
            }
        }
    }
    return nil
}

private func extractToFromSubject(json: NSMutableDictionary)->(String,String, String){
    var to = ""; var from = ""; var subject = ""

    for dict in ((json["payload"])!["headers"] as! NSArray){
        if to == "" && (dict["name"] as? String) == "To"{
            to = (dict["value"] as? String)!
        }
        if from == "" && (dict["name"] as? String) == "From"{
            from = (dict["value"] as? String)!
        }
        if subject == "" && (dict["name"] as? String) == "Subject"{
            subject = (dict["value"] as? String)!
        }

    }
    return (to,from,subject)
}

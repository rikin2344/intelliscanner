//
//  Process.swift
//  IntelliScanner
//
//  Created by Rikin Desai on 9/23/15.
//  Copyright (c) 2015 Rikin. All rights reserved.
//

import Foundation


class Process {
    //Contains file name
    var spamList:[AnyObject]?
    var hamList:[AnyObject]?
    
    //contains email data
    var spamFiles = [AnyObject]()
    var hamFiles = [AnyObject]()
    
    //Contains training data
    var spamTrain = [AnyObject]()
    var hamTrain = [AnyObject]()

    //contains testing data
    var spamTest = [AnyObject]()
    var hamTest = [AnyObject]()
    
    var FEATURE_CHARS:[Character] = [";","(","[","!","$","#"]
    
    //careful: false means take all words(!)
    let FEAT_SEL = true
    let NFEATURES = 100
    
    //ignore words with total freq lower than thresh
    let RARE_THRESH:Double = 0.01
    
    //max distance of spamicity from .5
    let SPAMICITY_RADIUS:Double = 0.05
    let SPAMICITY_WEIGHT:Double = 0.5
    
    let spamPromFormat = "self ENDSWITH '_spam.txt'"
    let hamPromFormat = "self ENDSWITH '_ham.txt'"
    
    
    var documentDir:String?

    init(){
        
    }
    
    func getFileList(){
        
        NaiveMain().run()
        return
        
        let bundleRoot = NSBundle.mainBundle().bundlePath
        let fm:NSFileManager = NSFileManager.defaultManager()
        var dirContent = fm.contentsOfDirectoryAtPath(bundleRoot, error: nil)
        var spamFilter = NSPredicate(format: "self ENDSWITH '_spam.txt'")
        spamList = dirContent?.filter({ spamFilter.evaluateWithObject($0)})
        spamList = (spamList as! [String]).map({ ($0 as String).stringByReplacingOccurrencesOfString(".txt", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)})

        var hamFilter = NSPredicate(format: "self ENDSWITH '_ham.txt'")
        hamList = dirContent?.filter({ hamFilter.evaluateWithObject($0)})
        hamList = (hamList as! [String]).map({ ($0 as String).stringByReplacingOccurrencesOfString(".txt", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)})
        extractFiles()
    }
    
    func extractFiles(){
        if let hamList = hamList{
            for message in hamList as! [String]{
                let path = NSBundle.mainBundle().pathForResource(message, ofType: "txt")
                var error:NSError?
                if let content = String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil) {
                    hamFiles.append(content)
                }
            }
        }
        if let spamList = spamList{
            for message in spamList as! [String]{
                let path = NSBundle.mainBundle().pathForResource(message, ofType: "txt")
                var error:NSError?
                if let content = String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil) {
                    spamFiles.append(content)
                }
            }
        }
        
        singleRun()
    }
    
    func split_Train_Test(var dataToSplit: [AnyObject])->([AnyObject],[AnyObject]){
        var a = Array(dataToSplit[0..<dataToSplit.count*2/3])
        return (Array(dataToSplit[0..<dataToSplit.count*2/3]),Array(dataToSplit[dataToSplit.count*2/3..<dataToSplit.count]))
    }
    
    func singleRun(){
        (spamTrain,spamTest) = split_Train_Test(spamFiles)
        (hamTrain, hamTest) = split_Train_Test(hamFiles)
        
        var (hamOut, spamOut, features) = self.processSet(hamFiles: hamTrain, spamFiles: spamTrain)
        
        var (ham, ham_total) = process_class(hamTest as! [String])
        var (spam, spam_total) = process_class(spamTest as! [String])
        
        var test_ham = prepareOutput(ham, features: features, constant_feature: "0")
        var test_spam = prepareOutput(spam, features: features, constant_feature: "1")
        
        var train = hamOut + spamOut
        var test = test_ham + test_spam
        
        train = shuffle(train)
        test = shuffle(test)
        
        for x in ["train", "test"] {
            if x == "train"{
                var data = ""
                var file = "train.txt"
                var path = documentDir!.stringByAppendingPathComponent(file)
                for line in train as [String]{
                    data += line
                }
                data.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
            }else{
                var data = ""
                var file = "test.txt"
                var path = documentDir!.stringByAppendingPathComponent(file)
                for line in test as [String]{
                    data += line
                }
                data.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
            }
            
        }
        
    }
    
    func processSet(#hamFiles: [AnyObject], spamFiles: [AnyObject])->([String],[String],[String]){
        var ranks = [String: Double]()
        var (ham, ham_total) = process_class(hamTrain as! [String])
        var (spam, spam_total) = process_class(spamTrain as! [String])
        
        var both_total = spam_total
        both_total.update(ham_total)
        
        for (key,frequency) in both_total {
            var ham_freq = ham_total[key] ?? 0
            var spam_freq = spam_total[key] ?? 0
            var spamicity = spam_freq / (spam_freq+ham_freq)
            var diff = abs(spam_freq - ham_freq)
            if(abs(spamicity - 0.5) > self.SPAMICITY_RADIUS) && frequency > self.RARE_THRESH{
                ranks[key] = diff
            }
        }
        
        var selected = sorted(ranks){ $0.1 > $1.1  }
        selected = Array(selected[0...149])
        var keyFeatures = self.createStatsFileForData(selected, ham_total: ham_total, spam_total: spam_total)
        
        var hamOut = prepareOutput(ham, features: keyFeatures!, constant_feature: "0")
        var spamOut = prepareOutput(spam, features: keyFeatures!, constant_feature: "1")
        
        return (hamOut, spamOut, keyFeatures!)
    }
    
    func createStatsFileForData(rankSelected: [(String, Double)], ham_total: [String: Double], spam_total: [String: Double])->[String]?{
        let file = "stats.txt"
        var features = [String]()
        var path:String?
        var dataToWrite = ""
        
        if let dirs : [String] = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String] {
            let dir = dirs[0] //documents directory
            path = dir.stringByAppendingPathComponent(file)
            self.documentDir = dir
        }

        if path == nil {
            println("PATH is nil")
            return nil
        }
        for (index, (key, value)) in enumerate(rankSelected){
            
            features.append(key)
            
            var ham_freq = ham_total[key] ?? 0
            var spam_freq = spam_total[key] ?? 0
            var spamicity = spam_freq / (spam_freq + ham_freq)
            dataToWrite += "\(key),\(ham_freq),\(spam_freq),\(value),\(spamicity)\n"
        }
        dataToWrite.writeToFile(path!, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
        
        return features
    }
    
    func prepareOutput(rows:[[String: Double]], features:[String], constant_feature: String)->[String]{
        var out = [String]()
        for row in rows{
            var out_row = [String]()
            for k in features{
                out_row.append("\(row[k] ?? 0),")
            }
            out_row.append(constant_feature)
            var out_row_string = "".join(out_row)
            out.append(out_row_string + "\n")
        }
        return out
    }
    
    
    
    func process_class(files: [String])->([[String: Double]],[String: Double]){
        println("Processing \(files.count) files")
        var all_words = [String: Double]()
        var data = [[String: Double]]()
        for (index, file) in enumerate(files){
            var words_gen = file.clean().words
            var words = [String: Double]()
            var fwords = [String: Double]()
            
            for word in words_gen{
                //var word = wrd.clean()
                if count(word) > 1 {
                    if let value  = words[word]{
                        words[word] = value + 1
                    }else{
                        words[word] = 1
                    }
                }
            }
            
            var nwords = (Array(words.values)).reduce(0, combine: +)
            for (key,value) in words{
                var tempValue = Double(value/nwords)
                fwords[key] = tempValue
            }
            
            var nchars:Double = 0.0
            var maxrun = 0
            var nruns = 0
            var sumruns:Double = 0.0
            var chars_found = [Character]()
            
            for line in file.componentsSeparatedByString("\n"){
                var runlen = 0
                for char in line{
                    nchars += 1
                    if(contains(FEATURE_CHARS, char)){
                        if let value  = words[toString(char)]{
                            words[toString(char)] = value + 1
                        }else{
                            words[toString(char)] = 1
                        }
                    }
                    
                    chars_found.append(char)
                    if(char.isupper()){
                        runlen += 1
                    }else if runlen > 0 {
                        maxrun = max(maxrun, runlen)
                        sumruns += Double(runlen)
                        nruns += 1
                        runlen = 0
                    }
                }
            }
            
            for k in chars_found{
                if let value = words[toString(k)]{
                    fwords[toString(k)] = Double(value) / nchars
                }
            }
            
//            fwords["capital_run_length_average"] = Double(sumruns)/Double(nruns)
//            fwords["capital_run_length_longest"] = Double(maxrun)
//            fwords["capital_run_length_total"] = Double(sumruns)

            for key in words.keys{
                words[key] = 1
            }
            
            all_words = addDict(all_words, words)
            data.append(fwords)
        }
        var nrows = Double(data.count)
        for (key,value) in all_words {
            var tempValue = Double(value) / nrows
            all_words[key] = tempValue
        }
        //ham,ham_total
        return (data, all_words)
        
    }
}